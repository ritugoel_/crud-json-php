<?php

	$servername = "localhost";
	$username = "root";
	$password = "";
	$dbname = "mydb";

	// Create connection
	$conn = new mysqli($servername, $username, $password ,$dbname);
	// Check connection
	if ($conn->connect_error) {
	    die("Connection failed: " . $conn->connect_error);
	} 


	$sqldata = "SELECT * FROM create_user";
	$rows = $conn->query($sqldata);	    	

	$result = new stdClass;
	$result->status = 1;
	$result->message = "data listing";

	if ($rows->num_rows > 0) {
	    // output data of each row
	    $result->data = [];
	    while($row = $rows->fetch_assoc()) { 
	    	$result->data[] = $row;
		}
	}
	$conn->close();

	echo json_encode($result);
?>