<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
	<table id="users-table" border='1'>
		<thead>
			<tr>
				<th>Name</th>
				<th>User Name</th>
				<th>gender</th>
				<th>Mobile No</th>
				<th>Date of birth</th>
				<th>Email</th>
				<th>location</th>
			</tr>
		</thead>
		<tbody></tbody>
	</table>
</body>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script>
	$(function() {
		getuserList();
	});
	function getuserList(){
		$.ajax({
			method  : 'get',
			url    : 'list-json.php'
		}).done(function(response) {
			response = $.parseJSON(response);
			//alert(response.data);
			if(response.status == 1) {
				var html = "";
				$.each(response.data , function($key,$data){
					html += '<tr><td>'+$data.first_name+'</td><td>'+$data.user_name+'</td><td>'+$data.gender+'</td><td>'+$data.mobile_no+'</td><td>'+$data.d_o_b+'</td><td>'+$data.email+'</td><td>'+$data.location+'</td></tr>';
				});
				$('#users-table tbody').html(html);
			}
		});
	}
</script>
</html>